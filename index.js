const http = require('http')
const fs = require('fs')
const jsonFile = require('./data.json');
const uuid = require('uuid')

function delay(seconds){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            resolve('success')
        },seconds*1000)
    })
}

const server = http.createServer((request,response)=>{
    if(request.method === 'GET'){
        if(request.url === '/html'){

            const data = fs.promises.readFile('./index.html','utf-8')
            data.then((result)=>{
                response.writeHead(200,{'Content-Type': 'text/html'})
                response.write(result)
                response.end()
            })
            .catch((err)=>{
                response.writeHead(500,{'Content-Type': 'text/plain'})
                response.end('Internal Server Error occurred while getting the html file')
            })

        } else if(request.url === '/json'){

            if(!jsonFile){
                response.writeHead(500,{'Content-Type': 'text/plain'})
                response.end('Internal Server Error occurred while getting the json file')
            } else {
                response.writeHead(200,{'Content-Type': 'text/json'})
                response.write(JSON.stringify(jsonFile))
                response.end()
            }

        } else if(request.url === '/uuid'){

            response.writeHead(200,{'Content-Type': 'text/plain'})
            let obj = {}
            obj['uuid'] = uuid.v4()
            response.write(JSON.stringify(obj))
            response.end()

        } else if(request.url.startsWith('/status/')){

            let statusCode = request.url.split('/')[2]
            if(Object.keys(http.STATUS_CODES).includes(statusCode)){
                response.writeHead(statusCode,{'Content-Type': 'text/plain'})
                response.write(`Status Code - ${statusCode}: ${http.STATUS_CODES[statusCode]}`)
            } else {
                response.writeHead(404,{'Content-Type': 'text/plain'})
                response.write(`Invalid status code. Please check your url and Try again.`)
            }
            response.end()

        } else if(request.url.startsWith('/delay/')){

            let seconds = request.url.split('/')[2]
            seconds = Number(seconds)
            if(!isNaN(seconds)){
                delay(seconds).then((res)=>{
                    response.writeHead(200,{'Content-Type': 'text/plain'})
                    response.write(res)
                    response.end()
                })
            } else {
                response.writeHead(404,{'Content-Type': 'text/plain'})
                response.end('Delay Seconds is not the type number.Please check our url and try again')
            }
            
        } else {
            response.writeHead(404,{'Content-Type': 'text/plain'})
            response.end(' Invalid URL. Please check your url once and Try again.')
        }
    } else {
        response.writeHead(404,{'Content-Type': 'text/plain'})
        response.end('Only GET request are allowed')
    }
})
server.on('error',(error)=>{
    console.log(error);
})

server.listen(8000,()=>{
    console.log('server');
})